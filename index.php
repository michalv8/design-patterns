<?php

declare(strict_types=1);

require_once 'vendor/autoload.php';

use DesignPatterns\StaticFactory\Pizza as PizzaStatic;
use DesignPatterns\SimpleFactory\SimplePizzaFactory;
use DesignPatterns\SimpleFactory\PizzaFactory;
use DesignPatterns\AbstractFactory\AbstractPizzaFactory;

// StaticFactory
$staticPizza = PizzaStatic::create();

$staticPizza->describe();

// SimpleFactory

$simplePizzaFactory = new SimplePizzaFactory();

$bigMafia = $simplePizzaFactory->create('mafia');

$bigMafia->describe();

// AbstractFactory

$chicagoPizzaFactory = AbstractPizzaFactory::createChicagoPizzaFactory();

$chicagoMafia = $chicagoPizzaFactory->create('mafia');

$chicagoMafia->describe();
