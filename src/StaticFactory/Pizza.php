<?php

declare(strict_types=1);

namespace DesignPatterns\StaticFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\DoughInterface;
use DesignPatterns\Model\PizzaDescribeTrait;
use DesignPatterns\Model\PizzaInterface;
use DesignPatterns\Model\Sauce;
use DesignPatterns\Model\SauceInterface;
use DesignPatterns\Model\Topping;
use DesignPatterns\Model\ToppingInterface;

class Pizza implements PizzaInterface
{
    use PizzaDescribeTrait;

    private $dough;
    private $sauce;
    private $toppings;
    private $price;

    /**
     * @param ToppingInterface[] $toppings
     */
    public function __construct(DoughInterface $dough, SauceInterface $sauce, array $toppings, string $price)
    {
        $this->dough = $dough;
        $this->sauce = $sauce;
        $this->toppings = $toppings;
        $this->price = $price;
    }

    public function getDough(): DoughInterface
    {
        return $this->dough;
    }

    public function getSauce(): SauceInterface
    {
        return $this->sauce;
    }

    public function getToppings(): array
    {
        return $this->toppings;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public static function create(): PizzaInterface
    {
        return new Pizza(
            new Dough('medium', 'thin', 'round'),
            new Sauce('tomato', 'mild'),
            [new Topping('mozarella', 'cheese', 1)],
            '12.34'
        );
    }
}