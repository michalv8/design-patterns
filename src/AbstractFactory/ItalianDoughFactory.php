<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\DoughInterface;

class ItalianDoughFactory implements DoughFactoryInterface
{
    public function create(): DoughInterface
    {
        return new Dough('medium', 'thin', 'round');
    }
}