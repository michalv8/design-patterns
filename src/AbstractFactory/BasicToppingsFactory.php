<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\Topping;
use DesignPatterns\Model\ToppingInterface;

class BasicToppingsFactory implements ToppingFactoryInterface
{
    public function create(string $name, string $type, int $amount): ToppingInterface
    {
        return new Topping($name, $type, $amount);
    }
}