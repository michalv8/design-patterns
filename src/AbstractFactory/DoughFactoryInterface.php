<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\DoughInterface;

interface DoughFactoryInterface
{
    public function create(): DoughInterface;
}