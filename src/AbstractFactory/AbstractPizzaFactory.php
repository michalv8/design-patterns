<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\PizzaInterface;

abstract class AbstractPizzaFactory
{
    public static function createChicagoPizzaFactory(): PizzaFactory
    {
        return new PizzaFactory(
            new AmericanDoughFactory(),
            new TomatoBasedSauceFactory(),
            new BasicToppingsFactory()
        );
    }

    public static function createItalianNonGlutenPizzaFactory(): PizzaFactory
    {
        return new PizzaFactory(
            new ItalianDoughFactory(),
            new TomatoBasedSauceFactory(),
            new NonGlutenToppingFactory()
        );
    }


    abstract public function create(string $name): PizzaInterface;
}