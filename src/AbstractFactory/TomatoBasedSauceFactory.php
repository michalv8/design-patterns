<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\Sauce;
use DesignPatterns\Model\SauceInterface;

class TomatoBasedSauceFactory implements SauceFactoryInterface
{
    public function create(string $type): SauceInterface
    {
        return new Sauce('tomato', $type);
    }
}