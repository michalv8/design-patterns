<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\SauceInterface;

interface SauceFactoryInterface
{
    public function create(string $type): SauceInterface;
}