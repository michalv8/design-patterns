<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\Pizza;
use DesignPatterns\Model\PizzaInterface;

class PizzaFactory extends AbstractPizzaFactory
{
    private $doughFactory;
    private $sauceFactory;
    private $toppingFactory;

    public function __construct(DoughFactoryInterface $doughFactory, SauceFactoryInterface $sauceFactory, ToppingFactoryInterface $toppingFactory)
    {
        $this->doughFactory = $doughFactory;
        $this->sauceFactory = $sauceFactory;
        $this->toppingFactory = $toppingFactory;
    }

    public function create(string $name): PizzaInterface
    {
        switch ($name) {
            case 'mafia':
                return new Pizza(
                    $this->doughFactory->create(),
                    $this->sauceFactory->create('hot'),
                    [
                        $this->toppingFactory->create('mozzarella', 'cheese', 1),
                        $this->toppingFactory->create('bacon', 'meat', 1),
                        $this->toppingFactory->create('ham', 'meat', 1),
                        $this->toppingFactory->create('onion', 'vegetable', 1),
                        $this->toppingFactory->create('mushrooms', 'vegetable', 1)
                    ],
                    '15.00'
                );
            default:
                return new Pizza(
                    $this->doughFactory->create(),
                    $this->sauceFactory->create('mild'),
                    [
                        $this->toppingFactory->create('mozzarella', 'cheese', 1),
                    ],
                    '12.34'
                );
        }
    }
}