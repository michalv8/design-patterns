<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\DoughInterface;

class AmericanDoughFactory implements DoughFactoryInterface
{
    public function create(): DoughInterface
    {
        return new Dough('big', 'thick', 'rectangle');
    }
}