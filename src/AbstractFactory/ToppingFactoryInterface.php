<?php

declare(strict_types=1);

namespace DesignPatterns\AbstractFactory;

use DesignPatterns\Model\ToppingInterface;

interface ToppingFactoryInterface
{
    public function create(string $name, string $type, int $amount): ToppingInterface;
}