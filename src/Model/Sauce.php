<?php

declare(strict_types=1);

namespace DesignPatterns\Model;

class Sauce implements SauceInterface
{
    private $base;
    private $type;

    public function __construct(string $base, string $type)
    {
        $this->base = $base;
        $this->type = $type;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function getType(): string
    {
        return $this->type;
    }
}