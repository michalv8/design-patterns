<?php

declare(strict_types=1);

namespace DesignPatterns\Model;

interface ToppingInterface
{
    public function getName(): string;

    public function getType(): string;

    public function getAmount(): int;

    public function hasGluten(): bool;
}