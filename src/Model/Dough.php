<?php

declare(strict_types=1);

namespace DesignPatterns\Model;

class Dough implements DoughInterface
{
    private $size;
    private $thickness;
    private $shape;

    public function __construct(string $size, string $thickness, string $shape)
    {
        $this->size = $size;
        $this->thickness = $thickness;
        $this->shape = $shape;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function getThickness(): string
    {
        return $this->thickness;
    }

    public function getShape(): string
    {
        return $this->shape;
    }
}