<?php

declare(strict_types=1);

namespace DesignPatterns\Model;

interface SauceInterface
{
    public function getBase(): string;

    public function getType(): string;
}