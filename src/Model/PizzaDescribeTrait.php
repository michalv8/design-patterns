<?php

declare(strict_types=1);

namespace DesignPatterns\Model;


trait PizzaDescribeTrait
{
    public function describe(): string
    {
        $toppings = implode(
            "\n",
            array_map(
                function (ToppingInterface $topping): string {
                    $hasGluten = $topping->hasGluten() ? 'with gluten' : 'gluten free';
                    return "        - {$topping->getAmount()} {$topping->getName()}, {$topping->getType()} {$hasGluten}";
                },
                $this->toppings
            )
        );

        $pizzaName = get_class();

        $description = <<<EOF
{$pizzaName} (price - {$this->price}):
    - dough - {$this->dough->getSize()}, {$this->dough->getThickness()}, {$this->dough->getShape()}
    - sauce - {$this->sauce->getBase()}, {$this->sauce->getType()}
    - toppings:
{$toppings}
\n
EOF;

        print_r($description);

        return $description;
    }
}