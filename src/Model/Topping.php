<?php

declare(strict_types=1);

namespace DesignPatterns\Model;

class Topping implements ToppingInterface
{
    private $name;
    private $type;
    private $amount;
    private $gluten;

    public function __construct(string $name, string $type, int $amount, bool $gluten = true)
    {
        $this->name = $name;
        $this->type = $type;
        $this->amount = $amount;
        $this->gluten = $gluten;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function hasGluten(): bool
    {
        return $this->gluten;
    }
}