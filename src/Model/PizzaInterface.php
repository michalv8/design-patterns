<?php

declare(strict_types=1);

namespace DesignPatterns\Model;

interface PizzaInterface
{
    public function getDough(): DoughInterface;

    public function getSauce(): SauceInterface;

    /**
     * @return ToppingInterface[]
     */
    public function getToppings(): array;

    public function getPrice(): string;

    public function describe(): string;
}