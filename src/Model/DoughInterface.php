<?php

declare(strict_types=1);

namespace DesignPatterns\Model;

interface DoughInterface
{
    public function getSize(): string;

    public function getThickness(): string;

    public function getShape(): string;
}