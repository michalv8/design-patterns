<?php


namespace DesignPatterns\SimpleFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\PizzaInterface;
use DesignPatterns\Model\Sauce;
use DesignPatterns\Model\Topping;
use DesignPatterns\Model\Pizza;

class SimplePizzaFactory
{
    public function create(string $name): PizzaInterface
    {
        switch ($name) {
            case 'mafia':
                return new Pizza(
                    new Dough('big', 'thin', 'round'),
                    new Sauce('chili', 'hot'),
                    [
                        new Topping('mozzarella', 'cheese', 1),
                        new Topping('onion', 'vegetables', 1),
                        new Topping('mushrooms', 'vegetables', 1),
                        new Topping('bacon', 'meat', 1),
                        new Topping('ham', 'meat', 1),
                    ],
                    '15.00'
                );
            case 'carbonarra':
                return new Pizza(
                    new Dough('small', 'thin', 'round'),
                    new Sauce('cream', 'mild'),
                    [
                        new Topping('mozzarella', 'cheese', 1),
                        new Topping('onion', 'vegetables', 1),
                        new Topping('bacon', 'meat', 1),
                    ],
                    '16.20'
                );
            default:
                return new Pizza(
                    new Dough('medium', 'thin', 'round'),
                    new Sauce('tomato', 'mild'),
                    [
                        new Topping('mozzarella', 'cheese', 1),
                    ],
                    '12.34'
                );
        }
    }

}