<?php

declare(strict_types=1);

namespace DesignPatterns\SimpleFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\Pizza;
use DesignPatterns\Model\Sauce;
use DesignPatterns\Model\Topping;

class BigMafiaPizza extends Pizza
{
    public function __construct()
    {
        parent::__construct(new Dough('big', 'thin', 'round'),
            new Sauce('chili', 'hot'),
            [
                new Topping('mozzarella', 'cheese', 1),
                new Topping('onion', 'vegetables', 1),
                new Topping('mushrooms', 'vegetables', 1),
                new Topping('bacon', 'meat', 1),
                new Topping('ham', 'meat', 1),
            ],
            '15.00');
    }
}