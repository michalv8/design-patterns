<?php

declare(strict_types=1);

namespace DesignPatterns\SimpleFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\Pizza;
use DesignPatterns\Model\Sauce;
use DesignPatterns\Model\Topping;

class MediumMargheritaPizza extends Pizza
{
    public function __construct()
    {
        parent::__construct(
            new Dough('medium', 'thin', 'round'),
            new Sauce('tomato', 'mild'),
            [
                new Topping('mozzarella', 'cheese', 1),
            ],
            '12.34'
        );
    }
}