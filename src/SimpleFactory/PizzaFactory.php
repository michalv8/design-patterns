<?php

declare(strict_types=1);

namespace DesignPatterns\SimpleFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\PizzaInterface;
use DesignPatterns\Model\Sauce;
use DesignPatterns\Model\Topping;
use DesignPatterns\Model\Pizza;

class PizzaFactory
{
    public function create(string $name): PizzaInterface
    {
        switch ($name) {
            case 'mafia':
                return new BigMafiaPizza();
            case 'carbonarra':
                return new SmallCarbonarraPizza();
            default:
                return new MediumMargheritaPizza();
        }
    }
}