<?php

declare(strict_types=1);

namespace DesignPatterns\SimpleFactory;

use DesignPatterns\Model\Dough;
use DesignPatterns\Model\Pizza;
use DesignPatterns\Model\Sauce;
use DesignPatterns\Model\Topping;

class SmallCarbonarraPizza extends Pizza
{
    public function __construct()
    {
        parent::__construct(
            new Dough('small', 'thin', 'round'),
            new Sauce('cream', 'mild'),
            [
                new Topping('mozzarella', 'cheese', 1),
                new Topping('onion', 'vegetables', 1),
                new Topping('bacon', 'meat', 1),
            ],
            '16.20'
        );
    }
}